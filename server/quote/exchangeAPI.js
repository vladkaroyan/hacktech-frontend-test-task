import axios from "axios";

const { EXCHANGE_API_URL = "", EXCHANGE_API_KEY = "" } = process.env;

export const getQuoteData = async (queryData) => {
  // Always use the default values to prevent exceptions with the nullable values on the back end side
  const { from_currency_code = "", to_currency_code: currency_code = "" } =
    queryData || {};

  const apiResponse = await axios.get(
    `${EXCHANGE_API_URL}?apikey=${EXCHANGE_API_KEY}&base_currency=${from_currency_code}`
  );
  const { data: { [currency_code]: exchange_rate = 0 } = {} } =
    apiResponse.data;
  return exchange_rate;
};
