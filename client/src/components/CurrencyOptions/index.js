import React, { useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";

import classNames from "classnames";

import { currencyOptions } from "constants/currency";
import { changeCurrentSelection } from "store/currency/currencySlice";

import ExchangeIcon from "assets/icons/exchange.svg";
import LoaderIcon from "assets/icons/loader.svg";

const CurrencyOptions = () => {
  const {
    currentSelectionData: {
      from_currency_code = "",
      amount = 0,
      to_currency_code = "",
    } = {},
    quoteResponseData: { amount: total_amount, exchange_rate },
    getQuotePending = false,
  } = useSelector((state) => state.currency);

  const { logo: fromCurrencyLogo = "" } =
    currencyOptions[from_currency_code] || {};
  const { logo: toCurrencyLogo = "" } = currencyOptions[to_currency_code] || {};
  const amountText = useMemo(
    () => (
      <>
        <p>
          1 {from_currency_code} = {exchange_rate} {to_currency_code}
        </p>
        <p>
          {amount} {from_currency_code} = {total_amount} {to_currency_code}
        </p>
      </>
    ),
    // We need to listen only for total amount change to prevent data updating after request has already succeeded
    //  eslint-disable-next-line
    [total_amount]
  );
  const dispatch = useDispatch();

  const changeData = (key, value) => {
    dispatch(changeCurrentSelection({ [key]: value }));
  };

  const loaderClassNames = classNames("loading-spinner-wrapper", {
    "opacity-0": !getQuotePending,
  });

  return (
    <>
      <div className="amount">
        <p>Enter Amount</p>
        <div className="d-flex">
          <input
            type="number"
            value={amount}
            className="mr-10"
            onChange={(e) => changeData("amount", e.target.value)}
          />
          <div className={loaderClassNames}>
            <img className="loading-spinner" src={LoaderIcon} alt="Loader" />
          </div>
        </div>
      </div>

      <div className="drop-list mr-10">
        <div className="from">
          <p>From</p>
          <div className="select-box">
            <img src={fromCurrencyLogo} alt="flag" />
            <select
              onChange={(e) => changeData("from_currency_code", e.target.value)}
              value={from_currency_code}
            >
              {Object.keys(currencyOptions).map((currencyName) => (
                <option key={`from-${currencyName}`} value={currencyName}>
                  {currencyName}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="icon d-flex">
          <img src={ExchangeIcon} width={20} alt="Exchange Logo" />
        </div>
        <div className="to">
          <p>To</p>
          <div className="select-box">
            <img src={toCurrencyLogo} alt="flag" />
            <select
              onChange={(e) => changeData("to_currency_code", e.target.value)}
              value={to_currency_code}
            >
              {Object.keys(currencyOptions).map((currencyName) => (
                <option key={`to-${currencyName}`} value={currencyName}>
                  {currencyName}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
      <div className="padding-t-30">
        {!!total_amount && !!amount?.length && <div>{amountText}</div>}
      </div>
    </>
  );
};

export default CurrencyOptions;
