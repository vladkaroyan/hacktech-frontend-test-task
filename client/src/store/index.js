import { configureStore } from "@reduxjs/toolkit";
import currencySlice from "store/currency/currencySlice";

export default configureStore({
  reducer: {
    currency: currencySlice,
  },
});
