import * as yup from "yup";

const currencyOptions = ["USD", "EUR", "ILS"];

export const getQuoteQuerySchema = yup.object().shape({
  from_currency_code: yup.string().required().oneOf(currencyOptions),
  amount: yup.number().required().positive().integer(),
  to_currency_code: yup.string().required().oneOf(currencyOptions),
});
