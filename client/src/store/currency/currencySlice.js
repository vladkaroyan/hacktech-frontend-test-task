import { createSlice } from "@reduxjs/toolkit";
import { getQuote } from "api/currencyExchangeAPI";

const initialState = {
  currentSelectionData: {
    from_currency_code: "USD",
    amount: "",
    to_currency_code: "EUR",
  },
  getQuotePending: false,
  getQuoteRejected: false,
  getQuoteSuccess: false,
  quoteResponseData: {},
};

export const currencySlice = createSlice({
  name: "currency",
  initialState: {
    ...initialState,
  },
  reducers: {
    changeCurrentSelection: (state, action) => {
      const { payload: changedData } = action;
      return {
        ...state,
        currentSelectionData: {
          ...state.currentSelectionData,
          ...changedData,
        },
      };
    },
    resetQuoteResponseData: (state) => {
      return {
        ...state,
        quoteResponseData: {},
      };
    },
  },
  extraReducers: {
    [getQuote.pending]: (state) => {
      return {
        ...state,
        getQuotePending: true,
        getQuoteRejected: false,
        getQuoteSuccess: false,
      };
    },
    [getQuote.rejected]: (state) => {
      return {
        ...state,
        getQuotePending: false,
        getQuoteRejected: true,
        getQuoteSuccess: false,
      };
    },
    [getQuote.fulfilled]: (state, action) => {
      const { payload: quoteResponseData } = action;

      return {
        ...state,
        getQuotePending: false,
        getQuoteRejected: false,
        getQuoteSuccess: true,
        quoteResponseData,
      };
    },
  },
});

// Action creators are generated for each case reducer function
export const { changeCurrentSelection, resetQuoteResponseData } =
  currencySlice.actions;

export default currencySlice.reducer;
