import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { toast, ToastContainer } from "react-toastify";

import { resetQuoteResponseData } from "store/currency/currencySlice";
import { debounce } from "helpers/debounce";
import { getQuoteQuerySchema, rejectMessage } from "constants/currency";
import { getQuote } from "api/currencyExchangeAPI";
import CurrencyOptions from "components/CurrencyOptions";

import "./index.css";

const ConverterBlock = () => {
  const {
    currentSelectionData: {
      from_currency_code = "",
      amount = 0,
      to_currency_code = "",
    } = {},
    getQuoteRejected = false,
  } = useSelector((state) => state.currency);
  const dispatch = useDispatch();

  // eslint-disable-next-line
  const getSelectionData = useCallback(
    debounce(async (requestParams) => {
      try {
        await getQuoteQuerySchema.validate(requestParams);
        dispatch(getQuote(requestParams));
      } catch (e) {
        toast.error(rejectMessage);
      }
    }, 500),
    []
  );

  useEffect(() => {
    if (amount?.length) {
      dispatch(resetQuoteResponseData());
      getSelectionData({
        from_currency_code,
        to_currency_code,
        amount,
      });
    }
    //  eslint-disable-next-line
  }, [from_currency_code, to_currency_code, amount]);

  useEffect(() => {
    if (getQuoteRejected) {
      toast.error(rejectMessage);
    }
    //  eslint-disable-next-line
  }, [getQuoteRejected]);

  return (
    <>
      <div className="wrapper">
        <header>Currency Converter</header>
        <CurrencyOptions />
      </div>
      <ToastContainer />
    </>
  );
};

export default ConverterBlock;
