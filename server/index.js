import "dotenv/config";
import express from "express";
import cors from "cors";
import NodeCache from "node-cache";
import { getQuote } from "./quote/getQuote.js";

const app = express();
export const quoteCacheData = new NodeCache({
  // Limit the cache timeout to 10 seconds
  stdTTL: 10,
  // Check the cache timeout every 1 second
  checkperiod: 1,
});

// Enable cors security headers
app.use(cors());

app.get("/quote", (req, res) => {
  const queryParams = req.query || {};
  getQuote(queryParams).then(
    (successResponse) => res.send(successResponse),
    (rejectResponse) => {
      console.error(rejectResponse);
      res.status(400);
      res.send(rejectResponse);
    }
  );
});

app.listen("3001", () => {});
