import CurrencyConverter from "./apps/CurrencyConverter";
import "./App.css";

const App = () => {
  return <CurrencyConverter />;
};

export default App;
