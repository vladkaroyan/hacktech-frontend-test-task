import { getQuoteQuerySchema } from "../schemas.js";
import { getQuoteData } from "./exchangeAPI.js";
import { quoteCacheData } from "../index.js";

export const getQuote = async (queryParams) => {
  await getQuoteQuerySchema.validate(queryParams, { abortEarly: true });

  // Always use the default values to prevent exceptions with the nullable values on the back end side
  const {
    from_currency_code = "",
    to_currency_code: currency_code = "",
    amount: exchange_amount,
  } = queryParams || {};

  // Define cache data key and get it from the cache
  const cacheDataKey = `${from_currency_code} => ${currency_code}`;
  const cacheExchangeData = quoteCacheData.get(cacheDataKey);

  // Define exchange_rate, set it 1 by default
  let exchange_rate = 1;

  // Check if From != To get data from the cache or from API
  if (from_currency_code !== currency_code) {
    exchange_rate = cacheExchangeData || (await getQuoteData(queryParams));
  }

  // If data in cache doesn't exist, just set it with the defined cache data key
  if (!cacheExchangeData) {
    quoteCacheData.set(cacheDataKey, exchange_rate);
  }

  const amount = (
    parseFloat(exchange_rate) * parseFloat(exchange_amount)
  ).toFixed(3);

  return {
    exchange_rate,
    currency_code,
    amount,
  };
};
