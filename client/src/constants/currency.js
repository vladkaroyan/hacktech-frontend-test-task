import * as yup from "yup";

export const currencyOptions = {
  USD: {
    logo: "https://flagcdn.com/48x36/us.png",
  },
  EUR: {
    logo: "https://flagcdn.com/48x36/eu.png",
  },
  ILS: {
    logo: "https://flagcdn.com/48x36/il.png",
  },
};

export const rejectMessage =
  "Something went wrong. Please check the validity of your data and try again!";

export const getQuoteQuerySchema = yup.object().shape({
  from_currency_code: yup
    .string()
    .required()
    .oneOf(Object.keys(currencyOptions)),
  amount: yup.string().required().min(1),
  to_currency_code: yup.string().required().oneOf(Object.keys(currencyOptions)),
});
