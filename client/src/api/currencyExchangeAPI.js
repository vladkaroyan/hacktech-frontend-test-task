import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getQuote = createAsyncThunk(
  "currency/getExchangeData",
  async (params, { rejectWithValue }) => {
    try {
      const { data } = await axios.get("/api/quote", { params });
      return data;
    } catch (e) {
      return rejectWithValue(e.response.data);
    }
  }
);
