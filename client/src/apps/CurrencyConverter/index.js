import React from "react";

import ConverterBlock from "containers/ConverterBlock";

const CurrencyConverter = () => {
  return <ConverterBlock />;
};

export default CurrencyConverter;
